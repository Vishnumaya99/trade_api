package com.example.demo;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Trade;
import com.example.demo.model.TradeState;
import com.example.demo.model.TradeType;
import com.example.demo.repository.Trade_Repository;

@RestController
@CrossOrigin
@RequestMapping("/trades")

public class Trade_Controller {
	@Autowired
	private Trade_Repository repository;
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Trade> getAllTrades() {
	  return repository.findAll();
	}
	
	@RequestMapping(value = "/addTrades", method = RequestMethod.GET)
	public void addTrades() {
	  Trade trade1 = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, "SBIN", 50, 1000.0);
	  repository.save(trade1);
	  Trade trade2 = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, "RELIANCE", 2, 2000.0);
	  repository.save(trade2);
	  Trade trade3 = new Trade(new ObjectId(), new Date(), TradeState.State.CREATED, TradeType.Type.BUY, "HDFCBANK", 10, 3000.0);
	  repository.save(trade3);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Trade getTradeById(@PathVariable("id") ObjectId id) {
	  return repository.findBy_id(id);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public void modifyTradeById(@PathVariable("id") ObjectId id, @Valid @RequestBody Trade trade) {
	  trade.set_id(id);
	  repository.save(trade);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Trade createTrade(@Valid @RequestBody Trade trade) {
	  trade.set_id(ObjectId.get());
	  trade.setCreated(new Date());
	  trade.setState(TradeState.State.CREATED);
	  trade.setType(TradeType.Type.BUY);
	  repository.save(trade);
	  return trade;
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public void deleteTrade(@PathVariable ObjectId id) {
	  repository.delete(repository.findBy_id(id));
	}
	
	@RequestMapping(value = "/{id}/updateStatus", method = RequestMethod.PUT)
	public ResponseEntity<Object> updateStatus(@PathVariable ObjectId id, @RequestParam String status) {
	   Trade trade = repository.findBy_id(id);
	   if(trade.getState().equals(TradeState.State.CREATED))
	   {
		   trade.setState(TradeState.State.valueOf(status));
		   repository.save(trade);
		   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
	   }
	   else if(trade.getState().equals(TradeState.State.PROCESSING))
	   {
		   if(!status.equals("CREATED"))
		   {
			   trade.setState(TradeState.State.valueOf(status));
			   repository.save(trade);
			   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
		   }
		   else
		   {
			   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
		   }
	   }
	   else if(trade.getState().equals(TradeState.State.FILLED))
	   {
		   if(!(status.equals("CREATED") || status.equals("PENDING")))
		   {
			   trade.setState(TradeState.State.valueOf(status));
			   repository.save(trade);
			   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
		   }
		   else
		   {
			   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
		   }
	   }
	   else if(trade.getState().equals(TradeState.State.FILLED))
	   {
		   if(!(status.equals("CREATED") || status.equals("PROCESSING") || status.equals("FILLED")))
		   {
			   trade.setState(TradeState.State.valueOf(status));
			   repository.save(trade);
			   return new ResponseEntity<>("Trade status updated successfully", HttpStatus.OK);
		   }
		   else
		   {
			   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
		   }
	   }
	   else {
		   return new ResponseEntity<>("Invalid operation", HttpStatus.FORBIDDEN);
	   }
//	   repository.save(trade);
//	   return trade;
	}


}
